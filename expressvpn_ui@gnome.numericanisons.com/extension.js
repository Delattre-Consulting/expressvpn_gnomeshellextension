const St = imports.gi.St;
const Main = imports.ui.main;
const Lang = imports.lang;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Panel = imports.ui.panel;
const GLib = imports.gi.GLib;

const ExpressVPN_UI = new Lang.Class({

	Name: 'ExpressVPN_UI',
	Extends: PanelMenu.Button,

	isConnected: false,
	city: null,

	_init: function () {
		this.parent(0.0, "ExpressVPN UI", false);

		// Icone in top bar
		let text = new St.Label({text: "EV"});
		this.actor.add_actor(text);

		// Menu : first creation
		this._recreateMenu();

		//update every <frequence> seconds
		let frequence = 10;
		event = GLib.timeout_add_seconds(0, frequence, Lang.bind(this, function ()
							{
								// Recreate the menu each <frequence> seconds
                this._recreateMenu();
                return true;
							})
						);
 	},

	/*
	 * Method to create and recreate the menu.
	 * Remove the previous one, update the state of the connection and then create the menu
	 */
	_recreateMenu: function() {

		// Clean old elements
		this.menu.removeAll();

		// recalculate State of the connection
		this._updateConnectionState();

		// Test of the state
		if (this.isConnected) {
			// Connected
			this._createMenu_stateConnected();
		}
		else {
			// Disconnected
			this._createMenu_stateDisconnected();
		}
	},

	/*
	 * Create a menu with two elements:
	 * - The state "Connected" and the country/city
	 * - An action button to disconnect the VPN
	 */
	_createMenu_stateConnected: function() {

		//
		this._menuElement_state = new PopupMenu.PopupMenuItem("Connected: " + this.city);
		this.menu.addMenuItem(this._menuElement_state);

		// Action: clic to disconnect
		this._menuElement_action = new PopupMenu.PopupMenuItem("Disconnect");
		this.menu.addMenuItem(this._menuElement_action);

		this._menuElement_action.connect(
			'activate',
			Lang.bind(this, function() {
				this._disconnect();
			})
		);
	},

	/*
	 * Create a menu with two elements:
	 * - The state "Disconnected"
	 * - An action button to connect the VPN to a smart location
	 */
	_createMenu_stateDisconnected: function() {
		this._menuElement_state = new PopupMenu.PopupMenuItem("Disconnected");
		this.menu.addMenuItem(this._menuElement_state);

		// Action: clic to connect
		this._menuElement_action = new PopupMenu.PopupMenuItem("Smart connect");
		this.menu.addMenuItem(this._menuElement_action);

		this._menuElement_action.connect(
			'activate',
			Lang.bind(this, function() {
				this._connect();
			})
		);
	},

	/*
	 * Update the status (isConected = true/flase) and the city if connected
	 */
	_updateConnectionState: function() {

		// Request terminal
		let commandeStatus = GLib.spawn_command_line_sync("expressvpn status");
		let commandeResult = commandeStatus[1].toString();

		// Check the status
		if (commandeResult.indexOf('Connected') == 0) {
			this.isConnected = true;
			this.city = commandeResult.split("to ")[1].replace(/(\r\n|\n|\r)/gm,"");
		}
		// By default: state is disconnected
		else {
			this.isConnected = false;
			this.city = null;
		}
	},

	/*
	 * Call a command to connect the VPN to a smart location
	 */
	_connect: function() {
		let commandeStatus = GLib.spawn_command_line_sync("expressvpn connect smart &");
		this._recreateMenu();
	},

	/*
	 * Call a command to disconnect the VPN
	 */
	_disconnect: function() {
		let commandeStatus = GLib.spawn_command_line_sync("expressvpn disconnect &");
		this._recreateMenu();
	}

});




let expressVPNMenu;
let event = null;

function init() {}

function enable() {

	// Create if null
	if (expressVPNMenu == null) {
		expressVPNMenu = new ExpressVPN_UI;
	}

	// Adding to the status bar
	Main.panel.addToStatusArea('expressVPN-indicator', expressVPNMenu);
}

function disable() {

	// Destroy if not null
	if (expressVPNMenu) {
		expressVPNMenu.destroy();
	}

	// Fully destroyed
	expressVPNMenu = null;

	// Removing event
	Mainloop.source_remove(event);
}
