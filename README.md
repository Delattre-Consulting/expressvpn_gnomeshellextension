# Gnome Shell Extension for ExpressVPN

This extension use the expressVPN commands to connect and disconnect the VPN without opening a terminal.

Extension developped and tested with "GNOME Shell 3.22.3" only.

Bugs known:
- On clic on "Smart connect", the computer can freeze for seconds while expressVPN establishes a connection.

